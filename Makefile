# Usage:
# make					# build
# make run			# run
# make clean		# remove ALL binaries and objects

#Windows detector - find rm in PATH
ifeq ($(wildcard $(addsuffix /rm,$(subst :, ,$(PATH)))),)
	WINMODE=1
else
	WINMODE=0
endif

.PHONY = all run clean

# Compiler to use
CXX = g++

# Flags
SRCS_FLAGS = -g -std=c++11 -Iinclude

# OBJS := helloworld.o



###########################################################
ifeq ($(WINMODE),1)  # native windows (BASH TERMINAL):
###########################################################

GTK_FLAGS = `pkgconf gtkmm-3.0 --cflags --libs`

all: Main.exe

run: Main.exe
	./$<

Main.exe: src/main.cpp $(OBJS)
	${CXX} ${SRCS_FLAGS} $< $(OBJS) -o $@ ${GTK_FLAGS}

clean:
	@echo "Cleaning up..."
	rm -f *.o Main

###########################################################
else #Unix like
###########################################################

GTK_FLAGS = `pkg-config gtkmm-3.0 --cflags --libs`

all: Main

run: Main
	./$<

Main: src/main.cpp $(OBJS)
	${CXX} ${SRCS_FLAGS} $< $(OBJS) -o $@ ${GTK_FLAGS}

clean:
	@echo "Cleaning up..."
	rm -f *.o Main

###########################################################
endif
###########################################################