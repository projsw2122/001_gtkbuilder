#include <iostream>
#include <gtkmm.h>

// Global variables (in a more refined example, these would be members of a class)
Gtk::Window *main_window;
Gtk::Entry *operand_1;
Gtk::Entry *operand_2;
Gtk::Entry *result;
Gtk::Button *bt_operator;
Gtk::Button *bt_result;


void on_button_result_clicked_x_();

int main(int argc, char **argv) {
  // Initialization
  main_window = nullptr;
  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create();

  // Load the GtkBuilder file and instantiate its widgets
  Glib::RefPtr<Gtk::Builder> builder = Gtk::Builder::create();

  try {
    builder->add_from_file("src/main.glade");
  } catch (const Glib::FileError &e) {
    std::cerr << "FileError: " << e.what() << std::endl;
    return -1;
  } catch(const Glib::MarkupError &e) {
    std::cerr << "MarkupError: " << e.what() << std::endl;
    return -1;
  } catch(const Gtk::BuilderError &e) {
    std::cerr << "BuilderError: " << e.what() << std::endl;
    return -1;
  }

  // Get the main window
  builder->get_widget("win_main", main_window);
  if (!main_window) {
    std::cerr << "Could get the main window widget" << std::endl;
    return -1;
  }

  // Get widgets pointers
  operand_1 = nullptr;
  operand_2 = nullptr;
  result = nullptr;
  bt_operator = nullptr;
  bt_result = nullptr;

  builder->get_widget("entry_opd_1", operand_1);
  builder->get_widget("entry_opd_2", operand_2);
  builder->get_widget("button_opt" , bt_operator);
  builder->get_widget("entry_result" , result);
  builder->get_widget("button_result", bt_result);

  if (!operand_1 || !operand_2 || !bt_operator || !result || !bt_result) {
    std::cerr << "Could get the buttons and entrys widgets" << std::endl;
    return -1;
  }

  // Connect signal handlers
  bt_result->signal_clicked().connect(sigc::ptr_fun(on_button_result_clicked_x_));

  // Run application
  app->run(*main_window);

  delete operand_1;
  delete operand_2;
  delete bt_operator;
  delete result;
  delete bt_result;

  std::cout << "Bye!!!" << std::endl;
  return 0;
}

void on_button_result_clicked_x_() {
  // Get text from operator 1 and 2
  std::string opr_1_txt = operand_1->get_text();
  std::string opr_2_txt = operand_2->get_text();

  // string > double
  double opr_1, opr_2;
  try {
    opr_1 = std::stod(opr_1_txt);
    opr_2 = std::stod(opr_2_txt);
  } catch (std::invalid_argument &e) {
    std::cout << "Invalid operands!!!" << std::endl;
    return;
  }

  // Compute result
  double rst = opr_1 + opr_2;
  result->set_text( std::to_string(rst) );

  std::cout << "Operand 1: " << opr_1 << std::endl
            << "Operand 2: " << opr_2 << std::endl
            << "Operator : " << bt_operator->get_label() << std::endl
            << "Result: " << rst << std::endl << std::endl;
}
