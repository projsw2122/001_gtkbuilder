# 001_GtkBuilder

This project is a simple example of how to use `Gtk::Builder` to load an
existent `.glade` file (interface designed in Glade) and use it for build your
application's interface in runtime.

The example consistent on a simple sum calculator where the interface has 2 text
entries for 2 operands and 1 entry for the result, and two buttons (operator and
to obtain the result). When the result's button is pressed, the result is also
printed in `std::cout`.

As mentioned in the Roadmap, a miniquest can be developed in the future based
on the `001_GtkBuilder` example.

## Installation

This repository requires the configuration of GTK + Glade developer environment
for Linux or Windows. Please access the respective GitLab
[repo](https://gitlab.com/sousarbarb_feup_courses/psw/cpp/config/gtkglade) for
installing these tools.

## Usage

### Linux

```sh
# Clean all the binaries and objects present in the workspace
make clean
# Build the executable file (optional - run already build the executable)
make
# Run
make run
```

### Windows

Execute the following commands in a bash terminal (e.g., VSCode > Terminal >
New terminal > `+` > bash) for openning a MinGW terminal:

```sh
# Clean all the binaries and objects present in the workspace
mingw32-make clean
# Build the executable file (optional - run already build the executable)
mingw32-make
# Run
mingw32-make run
```

## Getting started with Glade

### New project

1. Open Glade
2. Create new project
3. Toplevels > GtkWindow
4. Containers > GtkFixed > Click over the added window
5. Add control widgets:

   - Button: Control > GtkButton
   - Text entry: Control > GtkEntry

6. Change the widgets' properties on the right-side ribbon

### Existent project

1. Open Glade
2. Click on Open and open the `.glade` file

### Tips

- Move and resize objects: `Shift + press left mouse button` on the object
- Main window with fixed size:

  - Select GtkWindow widget
  - General > Disable `Resizable`
  - Common > Enable width and height requests and set to the desired size

## Support

Please contact Ricardo B. Sousa (rbs@fe.up.pt) if you have any questions.

## Roadmap

- Miniquest

  - Probably, it will be developed in a separated project
  
  1. Use Glade to design the interface and generate the `.glade` file
  2. Develop a program to load the `.glade` file from the workspace and open the
     interface designed in Glade (using the `Gtk::Builder`)
  3. Code refactor for using C++ classes (this step not implemented in this
     repository)
  4. Implement sum operation (including the handler for the result button)
  5. Implement a handler for the operator button to support `-`, `/`, and `*`
     (this step and further not implemented in this repository)
  6. Get `.glade` file directory from `argc` and `argv` (not necessarily the
     last step)

## Authors

- Ricardo B. Sousa - [email](mailto:rbs@fe.up.pt)

- Armando Sousa - [email](mailto:asousa@fe.up.pt)
